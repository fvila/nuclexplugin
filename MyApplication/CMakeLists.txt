include_directories (${NuclexPluginArchitecture_SOURCE_DIR}/MyEngine)

set (MyApplication_SOURCES
    MyApplication.cpp
)

set (MyApplication_HEADERS
)

add_executable (MyApplication ${MyApplication_SOURCES} ${MyApplication_HEADERS})
target_link_libraries (MyApplication MyEngine)
target_link_libraries (MyApplication ${CMAKE_DL_LIBS})
