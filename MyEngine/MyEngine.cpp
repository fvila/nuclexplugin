///////////////////////////////////////////////////////////////////////////////
// Plugin architecture example                                               //
//                                                                           //
// This code serves as an example to the plugin architecture discussed in    //
// the article and can be freely used                                        //
///////////////////////////////////////////////////////////////////////////////
#define MYENGINE_SOURCE 1

#include "Config.h"
#include "Kernel.h"

using namespace MyEngine;
