///////////////////////////////////////////////////////////////////////////////
// Plugin architecture example                                               //
//                                                                           //
// This code serves as an example to the plugin architecture discussed in    //
// the article and can be freely used.                                       //
///////////////////////////////////////////////////////////////////////////////
#define OPENGLPLUGIN_SOURCE 1

#include "Config.h"
#include "../MyEngine/Kernel.h"

using namespace std;

namespace MyEngine {

  /// OpenGL graphics drver
  class OpenGLGraphicsDriver : public GraphicsServer::GraphicsDriver {

    /// <summary>Destroys an OpenGL graphics driver</summary>
    public: OPENGLPLUGIN_API virtual ~OpenGLGraphicsDriver() {}

    /// <summary>Gets the name of the graphics driver</summary>
    public: OPENGLPLUGIN_API virtual const std::string &getName() const {
      static string sName("OpenGL graphics driver");
      return sName;
    }

    /// <summary>Creates a renderer</summary>
    auto_ptr<Renderer> createRenderer() {
      return auto_ptr<Renderer>(new Renderer());
    }

  };

  /// <summary>Retrieve the engine version we're going to expect</summary>
  extern "C" OPENGLPLUGIN_API int getEngineVersion() {
    return 1;
  }

  /// <summary>Register the plugin to an engine kernel</summary>
  /// <param name="kernel">Kernel the plugin will register to</summary>
  extern "C" OPENGLPLUGIN_API void registerPlugin(Kernel &kernel) {
    kernel.getGraphicsServer().addGraphicsDriver(
      auto_ptr<GraphicsServer::GraphicsDriver>(new OpenGLGraphicsDriver())
    );
  }

} // namespace MyEngine