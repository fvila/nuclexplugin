///////////////////////////////////////////////////////////////////////////////
// Plugin architecture example                                               //
//                                                                           //
// This code serves as an example to the plugin architecture discussed in    //
// the article and can be freely used.                                       //
///////////////////////////////////////////////////////////////////////////////
#define ZIPPLUGIN_SOURCE 1

#include "Config.h"
#include "../MyEngine/Kernel.h"

#include <stdexcept>

using namespace std;

namespace MyEngine {

  /// Zip archive reader
  class ZipArchiveReader : public StorageServer::ArchiveReader {

    /// <summary>Destroys a zip archive reader</summary>
    public: ZIPPLUGIN_API virtual ~ZipArchiveReader() {}

    /// <summary>
    ///   Checks whether the archive reader can open an archive
    /// </summary>
    /// <param name="filename">Archive that will be tested</param>
    public: ZIPPLUGIN_API bool canOpenArchive(const string &filename) {
      return filename.find(".zip") == (filename.length() - 4);
    }

    /// <summary>Opens an archive for reading</summary>
    /// <param name="filename">Archive that will be opened</param>
    public: ZIPPLUGIN_API auto_ptr<Archive> openArchive(
      const string &sFilename
    ) {
      if(!canOpenArchive(sFilename)) {
        throw runtime_error("No Zip archive");
      }

      return auto_ptr<Archive>(new Archive());
    }

  };

  /// <summary>Retrieves the engine version we're expecting</summary>
  /// <returns>The engine version the plugin was built against</returns>
  extern "C" ZIPPLUGIN_API int getEngineVersion() {
    return 1;
  }

  /// <summary>Registers the plugin to an engine kernel</summary>
  /// <param name="kernel">Kernel the plugin will be registered to</param>
  extern "C" ZIPPLUGIN_API void registerPlugin(Kernel &kernel) {
    kernel.getStorageServer().addArchiveReader(
      auto_ptr<StorageServer::ArchiveReader>(new ZipArchiveReader())
    );
  }

} // namespace MyEngine